# Fault Tolerance of Neural Networks in Adversarial Settings

Research Paper: [ArXiv](https://arxiv.org/abs/1910.13875)

The repository includes the code for the paper titled "Fault Tolerance of Neural Networks in Adversarial Settings" published in the [Journal of Intelligent and Fuzzy Systems](https://www.iospress.nl/journal/journal-of-intelligent-fuzzy-systems/).
All the code is in .ipynb notebooks to make it easier to reproduce the code.

The code is available in the "/Code" directory. The code is divided into different folders based on the experiments.

- "/Code/Adversarial Robustness:" The folder contains the code for adversarial training using TRADES algorithm of Neural Networks and evaluating the fault tolerance based on the generalization error.
    - "/Epsilon Variation/:" Experiments for evaluating the variation of generalization error (fault tolerance) with increasing the strength of the adversarial noise
    - "/Noise Experiments:" This explores the generalization effect of adding small amount of noise to inputs and increasing the strength.
    - The code corresponding to the two datasets: CIFAR10 and FashionMNIST are provided in the corresponding folder.

- "/Code/WTDist": This includes the code for estimating the standard deviation of the parameter value distribution for Adversarial Robust algorithms.

- "/Code/Privacy": The code for training Neural Network with Renyi Differential Privacy for both Convolutional and MLP neural networks.

- "/Code/Faults": This includes the experiment for evaluating the performance degradation for TRADES on Fashion MNIST dataset.

- "/Code/Results": The directory includes the training log for different hyperparameter values and results for all the experiments.