# -*- coding: utf-8 -*-
"""PGD_Yale.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1CyHNsfOU1gt1rVhB9jmZXv-YnQlZ1Wn6
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from datetime import datetime
import json
import os
import shutil
from timeit import default_timer as timer

import tensorflow as tf
import numpy as np


os.environ['CUDA_DEVICE_ORDER']="PCI_BUS_ID"
os.environ['CUDA_VISIBLE_DEVICES']='0' 
WIDTH_NUM = 2
import sys
import scipy as sp
import scipy.io as io

from google.colab import drive
drive.mount('/content/gdrive')
colab='Colab Notebooks'
path = F"/content/gdrive/My Drive/{colab}/YALEBXF.mat"

config=json.dumps({
  "_comment": "===== MODEL CONFIGURATION =====",
  "model_dir": "./model",

  "_comment": "===== TRAINING CONFIGURATION =====",
  "random_seed": 4557077,
  "max_num_training_steps": 20001,
  "num_schedule_steps": 1000,
  "num_output_steps": 100,
  "num_summary_steps": 500,
  "num_checkpoint_steps": 1000,
  "training_batch_size": 20,
  "step_size_schedule": [[0, 1e-4], [10000, 5e-5], [15000, 1e-5]],
  "weight_decay": 2e-4,
  "momentum": 0.9,

  "_comment": "===== EVAL CONFIGURATION =====",
  "num_eval_examples": 10000,
  "eval_batch_size": 200,
  "eval_on_cpu": False,

  "_comment": "=====ADVERSARIAL EXAMPLES CONFIGURATION=====",
  "epsilon": 0.0314,
  "k": 10,
  "a": 0.0078,
  "random_start": True,
  "loss_func": "xent",
  "store_adv_path": "attack.npy"
})

config=json.loads(config)

class LinfPGDAttack:
  def __init__(self, model, epsilon, k, a, random_start, loss_func):
    """Attack parameter initialization. The attack performs k steps of
       size a, while always staying within epsilon from the initial
       point."""
    self.model = model
    self.epsilon = epsilon
    self.k = k
    self.a = a
    self.rand = random_start

    if loss_func == 'xent':
      loss = model.xent
    elif loss_func == 'cw':
      label_mask = tf.one_hot(model.y_input,
                              10,
                              on_value=1.0,
                              off_value=0.0,
                              dtype=tf.float32)
      correct_logit = tf.reduce_sum(label_mask * model.pre_softmax, axis=1)
      wrong_logit = tf.reduce_max((1-label_mask) * model.pre_softmax, axis=1)
      loss = -tf.nn.relu(correct_logit - wrong_logit + 50)
    else:
      print('Unknown loss function. Defaulting to cross-entropy')
      loss = model.xent

    self.grad = tf.gradients(loss, model.x_input)[0]

  def perturb(self, x_nat, y, sess):
    """Given a set of examples (x_nat, y), returns a set of adversarial
       examples within epsilon of x_nat in l_infinity norm."""
    if self.rand:
      x = x_nat + np.random.uniform(-self.epsilon, self.epsilon, x_nat.shape)
    else:
      x = np.copy(x_nat)

    for i in range(self.k):
      grad = sess.run(self.grad, feed_dict={self.model.x_input: x,
                                            self.model.y_input: y})

      x += self.a * np.sign(grad)

      x = np.clip(x, x_nat - self.epsilon, x_nat + self.epsilon) 
      x = np.clip(x, 0, 1) # ensure valid pixel range

    return x

def YALE_split(Yale_file, train_points_per_label = 50):
    YALE = io.loadmat(Yale_file)
    X = YALE['X']
    Y = YALE['Y']
    X = X.T/255.0
    X = X.reshape((2414,  168, 192)).swapaxes(1,2)
    Y = Y.flatten()

    train_data, train_label, test_data, test_label = [], [], [], []
    np.random.seed(0)
    label_count = 0
    for label in set(Y):
        label_idx = np.argwhere(Y==label).flatten()
        tot_num = len(label_idx)
        idx_permute = np.random.permutation(label_idx)
        train_data.append(X[idx_permute[:train_points_per_label]])
        train_label.append(np.repeat(label_count, train_points_per_label))
        test_data.append(X[idx_permute[train_points_per_label:]])
        test_label.append(np.repeat(label_count, tot_num - train_points_per_label))
        label_count += 1
    train_data = np.concatenate(train_data)
    test_data = np.concatenate(test_data)
    train_label = np.concatenate(train_label)
    test_label = np.concatenate(test_label)

    train_idx_permute = np.random.permutation(len(train_label))
    train_data = np.expand_dims(train_data[train_idx_permute], 3)
    train_label = train_label[train_idx_permute]

    test_idx_permute = np.random.permutation(len(test_label))
    test_data = np.expand_dims(test_data[test_idx_permute],3)
    test_label = test_label[test_idx_permute]
    return train_data, train_label, test_data, test_label

class Model(object):
    def __init__(self, width_num=1):
        self.width_num = width_num
        self._build_model()
        
        
    def _fully_connected(self, x, inter_dim, out_dim):
        """FullyConnected layer for final output."""
        num_non_batch_dimensions = len(x.shape)
        prod_non_batch_dimensions = 1
        for ii in range(num_non_batch_dimensions - 1):
              prod_non_batch_dimensions *= int(x.shape[ii + 1])
        x = tf.reshape(x, [tf.shape(x)[0], -1])
        w = tf.get_variable(
            'DW_0', [prod_non_batch_dimensions, inter_dim],
            initializer=tf.uniform_unit_scaling_initializer(factor=1.0))
        b = tf.get_variable('biases_0', [inter_dim],
                        initializer=tf.constant_initializer())
        x = tf.nn.xw_plus_b(x, w, b)
        x = tf.nn.relu(x)
        w1 = tf.get_variable(
            'DW_1', [inter_dim, out_dim],
            initializer=tf.uniform_unit_scaling_initializer(factor=1.0))
        b1 = tf.get_variable('biases_1', [out_dim],
                        initializer=tf.constant_initializer())
        
        return tf.nn.xw_plus_b(x, w1, b1)
    
    def _conv(self, name, x, filter_size, in_filters, out_filters, strides):
        """Convolution."""
        with tf.variable_scope(name):
             n = filter_size * filter_size * out_filters
             kernel = tf.get_variable(
                  'DW', [filter_size, filter_size, in_filters, out_filters],
                  tf.float32, initializer=tf.random_normal_initializer(
                      stddev=np.sqrt(2.0/n)))
             return tf.nn.conv2d(x, kernel, strides, padding='SAME')
    def _decay(self):
        """L2 weight decay loss."""
        costs = []
        for var in tf.trainable_variables():
            if var.op.name.find('DW') > 0:
                costs.append(tf.nn.l2_loss(var))
        return tf.add_n(costs)
            
    def _build_model(self):
        self.x_input = tf.placeholder(tf.float32, shape = [None, 192, 168, 1])
        self.y_input = tf.placeholder(tf.int64, shape = [None])
        x = self._conv('conv_0', self.x_input, 3, 1, 4*self.width_num, [1, 1, 1, 1])
        x = tf.nn.relu(x)
        x = self._conv('conv_1', x, 3, 4*self.width_num, 4*self.width_num, [1, 2, 2, 1])
        x = tf.nn.relu(x)
        #print(x.shape)
        x = self._conv('conv_2', x, 3, 4*self.width_num, 8*self.width_num, [1, 1, 1, 1])
        x = tf.nn.relu(x)
        x = self._conv('conv_3', x, 3, 8*self.width_num, 8*self.width_num, [1, 2, 2, 1])
        x = tf.nn.relu(x)
        #print(x.shape)
        x = self._conv('conv_4', x, 3, 8*self.width_num, 16*self.width_num, [1, 1, 1, 1])
        x = tf.nn.relu(x)
        x = self._conv('conv_5', x, 3, 16*self.width_num, 16*self.width_num, [1, 2, 2, 1])
        x = tf.nn.relu(x)
        #print(x.shape)
        x = self._conv('conv_6', x, 3, 16*self.width_num, 32*self.width_num, [1, 1, 1, 1])
        x = tf.nn.relu(x)
        x = self._conv('conv_7', x, 3, 32*self.width_num, 32*self.width_num, [1, 2, 2, 1])
        x = tf.nn.relu(x)
        #print(x.shape)
        
    
        with tf.variable_scope('logit'):
            self.pre_softmax = self._fully_connected(x, 200, 38)



        y_xent = tf.nn.sparse_softmax_cross_entropy_with_logits(
            labels=self.y_input, logits=self.pre_softmax)

        self.xent = tf.reduce_sum(y_xent)
        self.weight_decay_loss = self._decay()

        self.y_pred = tf.argmax(self.pre_softmax, 1)

        correct_prediction = tf.equal(self.y_pred, self.y_input)

        self.num_correct = tf.reduce_sum(tf.cast(correct_prediction, tf.int64))
        self.accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

class DataSubset(object):
    def __init__(self, xs, ys):
        self.xs = xs
        self.n = xs.shape[0]
        self.ys = ys
        self.batch_start = 0
        self.cur_order = np.random.permutation(self.n)

    def get_next_batch(self, batch_size, multiple_passes=True, reshuffle_after_pass=True):
        if self.n < batch_size:
            raise ValueError('Batch size can be at most the dataset size')
        if not multiple_passes:
            actual_batch_size = min(batch_size, self.n - self.batch_start)
            if actual_batch_size <= 0:
                raise ValueError('Pass through the dataset is complete.')
            batch_end = self.batch_start + actual_batch_size
            batch_xs = self.xs[self.cur_order[self.batch_start : batch_end], ...]
            batch_ys = self.ys[self.cur_order[self.batch_start : batch_end], ...]
            self.batch_start += actual_batch_size
            return batch_xs, batch_ys
        actual_batch_size = min(batch_size, self.n - self.batch_start)
        if actual_batch_size < batch_size:
            if reshuffle_after_pass:
                self.cur_order = np.random.permutation(self.n)
            self.batch_start = 0
        batch_end = self.batch_start + batch_size
        batch_xs = self.xs[self.cur_order[self.batch_start : batch_end], ...]
        batch_ys = self.ys[self.cur_order[self.batch_start : batch_end], ...]
        self.batch_start += batch_size
        return batch_xs, batch_ys

tf.set_random_seed(config['random_seed'])

max_num_training_steps = config['max_num_training_steps']
num_output_steps = config['num_output_steps']
num_summary_steps = config['num_summary_steps']
num_checkpoint_steps = config['num_checkpoint_steps']
batch_size = config['training_batch_size']

# Setting up the data and the model
train_data, train_label, test_data, test_label = YALE_split(path)
print(train_data.shape, test_data.shape, np.amax(train_data), np.amin(test_data))
YALE_TRAIN = DataSubset(train_data,train_label)
YALE_TEST = DataSubset(test_data,test_label)


global_step = tf.contrib.framework.get_or_create_global_step()
model = Model(width_num=2)

# Setting up the optimizer
step_size_schedule = config['step_size_schedule']
weight_decay = config['weight_decay']
boundaries = [int(sss[0]) for sss in step_size_schedule]
boundaries = boundaries[1:]
values = [sss[1] for sss in step_size_schedule]
learning_rate = tf.train.piecewise_constant(
    tf.cast(global_step, tf.int32),
    boundaries,
    values)
total_loss = model.xent + weight_decay * model.weight_decay_loss
train_step = tf.train.AdamOptimizer(learning_rate).minimize(total_loss,
                                                   global_step=global_step)

# Set up adversary
adv_k = config['k']
adv_random_start = config['random_start']
adv_loss_func = config['loss_func']

# Setting up the Tensorboard and checkpoint outputs
#model_dir = config['model_dir']+'_robust'
#if not os.path.exists(model_dir):
#  os.makedirs(model_dir)

# We add accuracy and xent twice so we can easily make three types of
# comparisons in Tensorboard:
# - train vs eval (for a single run)
# - train of different runs
# - eval of different runs

#saver = tf.train.Saver(max_to_keep=None)
#tf.summary.scalar('accuracy adv train', model.accuracy)
#tf.summary.scalar('accuracy adv', model.accuracy)
#tf.summary.scalar('xent adv train', model.xent / batch_size)
#tf.summary.scalar('xent adv', model.xent / batch_size)
#tf.summary.image('images adv train', model.x_image)
#merged_summaries = tf.summary.merge_all()

#shutil.copy('config.json', model_dir)

eps_step = np.linspace(0,config['epsilon'],config['num_schedule_steps'])

with tf.Session() as sess:
  # Initialize the summary writer, global variables, and our time counter.
  #summary_writer = tf.summary.FileWriter(model_dir, sess.graph)
  sess.run(tf.global_variables_initializer())
  training_time = 0.0

  # Main training loop
  for ii in range(max_num_training_steps):
    ###########################################################################
    if ii<config['num_schedule_steps']:
        current_eps = eps_step[ii]
        attack = LinfPGDAttack(model, 
                       current_eps,
                       config['k'],
                       current_eps*2.0/config['k'],
                       config['random_start'],
                       config['loss_func'])
    #else:
    #    current_eps = config['epsilon']
    #print(current_eps)
    
    ############################################################################
    x_batch, y_batch = YALE_TRAIN.get_next_batch(batch_size)

    # Compute Adversarial Perturbations
    start = timer()
    x_batch_adv = attack.perturb(x_batch, y_batch, sess)
    end = timer()
    training_time += end - start

    nat_dict = {model.x_input: x_batch,
                model.y_input: y_batch}

    adv_dict = {model.x_input: x_batch_adv,
                model.y_input: y_batch}

    # Output to stdout
    if ii % 100 == 0:
      nat_acc = sess.run(model.accuracy, feed_dict=nat_dict)
      adv_acc = sess.run(model.accuracy, feed_dict=adv_dict)
      print('Step {}:    ({})'.format(ii, datetime.now()))
      print('    training nat accuracy {:.4}%'.format(nat_acc * 100))
      print('    training adv accuracy {:.4}%'.format(adv_acc * 100))
      if ii != 0:
        print('    {} examples per second'.format(
            num_output_steps * batch_size / training_time))
        training_time = 0.0
    # Tensorboard summaries
    if ii % 100 == 0:
    #  summary = sess.run(merged_summaries, feed_dict=adv_dict)
    #  summary_writer.add_summary(summary, global_step.eval(sess))
      ###################################print test accuracy##############
      test_acc = sess.run(model.accuracy, feed_dict={model.x_input:test_data,model.y_input:test_label})
      print('current test accuracy: ', test_acc)

    # Write a checkpoint
    #if ii % num_checkpoint_steps == 0:
    #  saver.save(sess,
    #             os.path.join(model_dir, 'checkpoint'),
    #             global_step=global_step)

    # Actual training step
    start = timer()
    sess.run(train_step, feed_dict=adv_dict)
    end = timer()
    training_time += end - start