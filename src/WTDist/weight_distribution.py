import matplotlib.pyplot as plt
import pandas as pd
from scipy.stats import gaussian_kde
import seaborn as sns
import statistics


parameters1=[]

def get_params(model):
  for param in model.parameters():
    if len(param.size())==4:
      print("Conv layer")
      for i in param:
        for j in i:
          for k in j:
            for l in k:
              parameters1.append(l.item())
    elif len(param.size())==2:
      print("FC Layer")
      for i in param:
        for j in i:
          #print(j)
          parameters1.append(j.item())
    
get_params(model)

def Average(lst): 
    return sum(lst) / len(lst) 

#print(parameters)
print("Std Dev of No Reg: ", statistics.stdev(parameters1))
print("Mean of No Reg: ", Average(parameters1) )

